package main

import (
    "fmt"
    "log"
    "net"
    "strings"
    "os"
    "flag"
)

func networkInterfaceDetails() {
        var count int

        ifaces, err := net.Interfaces()
        if err != nil {
                log.Print(fmt.Errorf("localAddresses: %v\n", err.Error()))
                return
        }

        for _, i := range ifaces {
                addrs, err := i.Addrs()
                if err != nil {
                        log.Print(fmt.Errorf("localAddresses: %v\n", err.Error()))
                        continue
                }

                for _, a := range addrs {
                        fmt.Printf("index=%d name=%v addr=%v mac=%v mtu=%v flags=%v", i.Index, i.Name, a, i.HardwareAddr, i.MTU, i.Flags )
                        if strings.Contains(i.Flags.String(), "up") {
                                fmt.Printf(" status=up\n")
                        } else {
                                fmt.Printf(" status=down\n")
                        }
                }
                count++
        }
        fmt.Println("Total interfaces:", count)
}

func main() {
        var ver = "1.0.0"
        var version bool
        flag.BoolVar(&version, "version", false, "version output")
        flag.Parse()
        if version {
            fmt.Println(os.Args[0] + " version is " + ver)
            os.Exit(0)
        }
        networkInterfaceDetails()
}
